from tkinter import *
from bs4 import BeautifulSoup
import requests
from PIL import Image

window = Tk()
window.title("Погода")
window.geometry('400x250')

def clicked():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.3'}

    url = ("https://www.google.com/search?q=Weather+"+txt.get()+"&hl=ru")
    html = requests.get(url, headers=headers)
    soup = BeautifulSoup(html.text,'lxml')

    try:
        location = soup.select('#wob_loc')[0].getText()
        time = soup.select('#wob_dts')[0].getText()
        weather = soup.select('#wob_dc')[0].getText()
        degrees = soup.select('#wob_tm')[0].getText()
       

        lbl.configure(text=location)

        weather_information_time = Label(window, text=time,width=400)
        weather_information_time.place(x=200, y=140, anchor="c")

        weather_information_weather = Label(window, text=weather,width=400)
        weather_information_weather.place(x=200, y=160, anchor="c")

        weather_information_degrees = Label(window, text=degrees+" °C",width=100) 
        weather_information_degrees.place(x=200, y=180, anchor="c")

    except (IndexError):
        Error_label = Label(window, text='Ошибка, введите город снова',height=4,width=400)
        Error_label.place(x=200, y=160, anchor="c")

name_of_gif = 'main.gif'
frames = Image.open(name_of_gif).n_frames
gif = [PhotoImage(file=name_of_gif,format=f"gif -index {i}") for i in range(frames)]
def animation(count):
    global anim
    gif2 = gif[count]

    gif_label.configure(image=gif2)
    count += 1
    if count == frames:
        count = 0
    anim = window.after(50,lambda :animation(count))

gif_label = Label(window,image="")
gif_label.pack()
animation(0)

lbl = Label(window, text="Введите город") 
lbl.place(x=200, y=15, anchor="c") 

txt = Entry(window, width=10)
txt.place(x=200, y=45, anchor="c") 

button_1 = Button(window, text="Поиск!", command=clicked)
button_1.place(x=200, y=75, anchor="c")

window.mainloop()